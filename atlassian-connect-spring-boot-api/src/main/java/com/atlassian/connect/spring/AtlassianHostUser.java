package com.atlassian.connect.spring;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Objects;
import java.util.Optional;

/**
 * The <a href="https://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/">Spring Security</a>
 * authentication principal for requests coming from an Atlassian host application in which the
 * add-on is installed.
 *
 * <p>This class is the Atlassian Connect equivalent of Spring Security's {@link User} and can be resolved as a Spring
 * MVC controller argument by using the {@link AuthenticationPrincipal} annotation:
 * <blockquote><pre><code> &#64;Controller
 * public class HelloWorldController {
 *
 *     &#64;RequestMapping(value = "/helloWorld", method = GET)
 *     public String (@AuthenticationPrincipal AtlassianHostUser hostUser) {
 *         ...
 *     }
 * }</code></pre></blockquote>
 *
 * <p>Outside of the context of a controller, the current authentication principal can be obtained programmatically via
 * {@link SecurityContextHolder}:
 * <blockquote><pre><code> Optional&lt;AtlassianHostUser&gt; optionalHostUser = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
 *         .map(Authentication::getPrincipal)
 *         .filter(AtlassianHostUser.class::isInstance)
 *         .map(AtlassianHostUser.class::cast);</code></pre></blockquote>
 *
 * @since 1.0.0
 */
public final class AtlassianHostUser {

    private AtlassianHost host;

    private Optional<String> userKey;

    /**
     * Creates a new empty Atlassian host principal (used for deserialization).
     */
    protected AtlassianHostUser() {}

    /**
     * Creates a new Atlassian host principal.
     *
     * @param host the host from which the request originated
     * @param optionalUserKey the key of the user on whose behalf a request was made (optional)
     */
    public AtlassianHostUser(AtlassianHost host, Optional<String> optionalUserKey) {
        this.host = host;
        this.userKey = optionalUserKey;
    }

    /**
     * Returns the host from which the request originated.
     *
     * @return the Atlassian host
     */
    public AtlassianHost getHost() {
        return host;
    }

    /**
     * The the key of the user on whose behalf a request was made.
     *
     * @return the user key
     */
    public Optional<String> getUserKey() {
        return userKey;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AtlassianHostUser that = (AtlassianHostUser) o;
        return Objects.equals(host, that.host) && Objects.equals(userKey, that.userKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(host, userKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("AtlassianHostUser{host=%s, userKey='%s'}", host, userKey.orElse("null"));
    }
}
