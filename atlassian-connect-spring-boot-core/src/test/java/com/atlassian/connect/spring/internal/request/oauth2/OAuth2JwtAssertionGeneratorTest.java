package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.text.ParseException;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OAuth2JwtAssertionGeneratorTest {

    private static final String SHARED_SECRET = "sssshhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsssshhhhhhhhhhhhhhhhhhhhhhhhhhhhhh";
    private static final String OAUTH_CLIENT_ID = "the-addons-oauth-client-id";
    public static final String USER_KEY = "user";
    public static final String AUTHORIZATION_SERVER_BASE_URL = "https://example.com";
    private static final String HOST_BASE_URL = "https://host.example.com/wiki";
    private OAuth2JwtAssertionGenerator oAuth2JwtAssertionGenerator;
    private AtlassianHost host;

    @Before
    public void beforeEach() {
        oAuth2JwtAssertionGenerator = new OAuth2JwtAssertionGenerator();
        host = mock(AtlassianHost.class);
        when(host.getSharedSecret()).thenReturn(SHARED_SECRET);
        when(host.getOauthClientId()).thenReturn(OAUTH_CLIENT_ID);
        when(host.getBaseUrl()).thenReturn(HOST_BASE_URL);
    }

    @Test
    public void generatesValidlySignedAssertions() throws ParseException, JOSEException {
        String assertion = oAuth2JwtAssertionGenerator.getAssertionString(
                new AtlassianHostUser(host, Optional.of(USER_KEY)),
                URI.create(AUTHORIZATION_SERVER_BASE_URL));
        Assert.assertThat(JWSObject.parse(assertion).verify(new MACVerifier(SHARED_SECRET)), is(true));
    }

    @Test
    public void includesRfcSpecifiedClaimsInAssertion() throws ParseException {
        String assertion = oAuth2JwtAssertionGenerator.getAssertionString(
                new AtlassianHostUser(host, Optional.of("user")),
                URI.create("https://example.com"));
        ReadOnlyJWTClaimsSet claims = JWTParser.parse(assertion).getJWTClaimsSet();
        assertThat("issuer is set to the client ID with appropriate prefix", claims.getIssuer(),
                is("urn:atlassian:connect:clientid:" + OAUTH_CLIENT_ID));
        assertThat("subject is set to the host user's user key", claims.getSubject(),
                is("urn:atlassian:connect:userkey:" + USER_KEY));
        assertThat("audience claim is a singleton String array", claims.getAudience(), iterableWithSize(1));
        assertThat("audience is set to the provided base url", claims.getAudience(),
                contains(AUTHORIZATION_SERVER_BASE_URL));
        assertThat("tnt is set to host base url", claims.getStringClaim("tnt"), is(HOST_BASE_URL));
    }
}