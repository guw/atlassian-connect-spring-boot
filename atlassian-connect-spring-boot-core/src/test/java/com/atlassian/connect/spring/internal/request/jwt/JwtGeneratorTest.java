package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import java.net.URI;

@RunWith(MockitoJUnitRunner.class)
public class JwtGeneratorTest {

    @InjectMocks
    private JwtGenerator jwtGenerator;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void shouldRejectRelativeUriWithoutHost() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("The given URI is not absolute");
        jwtGenerator.createJwtToken(HttpMethod.GET, URI.create("/api"));
    }

    @Test
    public void shouldRejectRelativeUriWithHost() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("The given URI is not absolute");
        jwtGenerator.createJwtToken(HttpMethod.GET, URI.create("/api"), new AtlassianHost());
    }

    @Test
    public void shouldFailOnHostUriMismatch() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("The given URI is not under the base URL of the given host");
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl("http://foo.atlassian.net/");
        jwtGenerator.createJwtToken(HttpMethod.GET, URI.create("http://example.com/api"), host);
    }
}
