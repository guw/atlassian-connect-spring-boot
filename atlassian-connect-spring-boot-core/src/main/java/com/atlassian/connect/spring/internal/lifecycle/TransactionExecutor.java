package com.atlassian.connect.spring.internal.lifecycle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

/**
 * A utility that executes transactions with optional rollback.
 */
@Service
public class TransactionExecutor {

    private static final Logger log = LoggerFactory.getLogger(TransactionExecutor.class);

    @Autowired
    private Optional<TransactionTemplate> optionalTransactionTemplate;

    /**
     * Executes the given supplier in a transaction, allowing the caller to trigger rollback before its completion
     * by setting the provided flag.
     *
     * @param supplier the function to execute
     * @param shouldRollback a flag indicating whether the transaction should be rolled back once the function has been executed
     * @param <T> the return type of the function
     * @return the value returned by the supplier
     */
    @Async
    public <T> ListenableFuture<T> executeWithRollbackOption(Supplier<T> supplier, AtomicBoolean shouldRollback) {
        TransactionCallback<T> callback = new TransactionCallbackWithTimeout<>(supplier, shouldRollback);
        T result = optionalTransactionTemplate.map(template -> template.execute(callback)).orElseGet(supplier);
        return AsyncResult.forValue(result);
    }

    private static class TransactionCallbackWithTimeout<T> implements TransactionCallback<T> {

        private final Supplier<T> supplier;

        private final AtomicBoolean shouldRollback;

        public TransactionCallbackWithTimeout(Supplier<T> supplier, AtomicBoolean shouldRollback) {
            this.supplier = supplier;
            this.shouldRollback = shouldRollback;
        }

        @Override
        public T doInTransaction(TransactionStatus status) {
            T result = supplier.get();
            if (shouldRollback.get()) {
                log.debug("Rolling back transaction.");
                status.setRollbackOnly();
            }
            return result;
        }
    }
}
