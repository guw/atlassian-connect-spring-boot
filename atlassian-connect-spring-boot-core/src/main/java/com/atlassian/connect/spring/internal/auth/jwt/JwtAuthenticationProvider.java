package com.atlassian.connect.spring.internal.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.CanonicalHttpRequest;
import com.atlassian.connect.spring.internal.jwt.CanonicalRequestUtil;
import com.atlassian.connect.spring.internal.jwt.HttpRequestCanonicalizer;
import com.atlassian.connect.spring.internal.jwt.JwtExpiredException;
import com.atlassian.connect.spring.internal.jwt.JwtParseException;
import com.atlassian.connect.spring.internal.jwt.JwtParser;
import com.atlassian.connect.spring.internal.jwt.JwtReader;
import com.atlassian.connect.spring.internal.jwt.JwtVerificationException;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * An {@link AuthenticationProvider} for JSON Web Tokens, issued by an Atlassian host or by the add-on itself.
 *
 * For JWTs issued by Atlassian hosts, in addition to verifying the signature of the JWT, the query string hash claim
 * specific to Atlassian Connect is also verified.
 */
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    private static final Class<JwtAuthenticationToken> TOKEN_CLASS = JwtAuthenticationToken.class;

    private AddonDescriptorLoader addonDescriptorLoader;

    private AtlassianHostRepository hostRepository;

    public JwtAuthenticationProvider(AddonDescriptorLoader addonDescriptorLoader, AtlassianHostRepository hostRepository) {
        this.addonDescriptorLoader = addonDescriptorLoader;
        this.hostRepository = hostRepository;
    }

    @Override
    public boolean supports(Class<?> authenticationClass) {
        return authenticationClass.equals(TOKEN_CLASS);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtCredentials jwtCredentials = getJwtCredentials(authentication);
        JWTClaimsSet unverifiedClaims = parseToken(jwtCredentials.getRawJwt());
        log.debug("Parsed JWT: {}", unverifiedClaims);
        String hostClientKey = getHostClientKeyFromSelfAuthenticationToken(unverifiedClaims)
                .orElseGet(unverifiedClaims::getIssuer);
        AtlassianHost host = getHost(hostClientKey);
        JWTClaimsSet verifiedClaims = verifyToken(jwtCredentials, host);
        return new JwtAuthentication(new AtlassianHostUser(host, Optional.ofNullable(verifiedClaims.getSubject())), verifiedClaims);
    }

    private JwtCredentials getJwtCredentials(Authentication authentication) {
        JwtAuthenticationToken authenticationToken = TOKEN_CLASS.cast(authentication);
        return authenticationToken.getCredentials();
    }

    private JWTClaimsSet parseToken(String jwt) throws AuthenticationException {
        try {
            return new JwtParser().parse(jwt);
        } catch (JwtParseException e) {
            log.error(e.getMessage());
            throw new InvalidJwtException(e.getMessage(), e);
        }
    }

    private Optional<String> getHostClientKeyFromSelfAuthenticationToken(JWTClaimsSet unverifiedClaims) {
        Optional<String> optionalClientKey = Optional.empty();
        String addonKey = addonDescriptorLoader.getDescriptor().getKey();
        if (addonKey.equals(unverifiedClaims.getIssuer())) {
            assertValidSelfAuthenticationTokenAudience(unverifiedClaims, addonKey);

            Object clientKeyClaim = unverifiedClaims.getCustomClaim(SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM);
            String clientKey = assertValidSelfAuthenticationTokenClientKey(clientKeyClaim);
            optionalClientKey = Optional.of(clientKey);
        }
        return optionalClientKey;
    }

    private void assertValidSelfAuthenticationTokenAudience(JWTClaimsSet unverifiedClaims, String addonKey) {
        List<String> audience = unverifiedClaims.getAudience();
        if (audience == null) {
            throw new BadCredentialsException("Missing audience for self-authentication token");
        }

        if (!audience.equals(Collections.singletonList(addonKey))) {
            throw new BadCredentialsException(String.format("Invalid audience (%s) for self-authentication token", String.join(",", audience)));
        }
    }

    private String assertValidSelfAuthenticationTokenClientKey(Object clientKeyClaim) {
        if (clientKeyClaim == null) {
            throw new BadCredentialsException("Missing client key claim for self-authentication token");
        }
        return clientKeyClaim.toString();
    }

    private AtlassianHost getHost(String clientKey) throws AuthenticationException {
        Optional<AtlassianHost> potentialHost = Optional.ofNullable(hostRepository.findOne(clientKey));
        if (!potentialHost.isPresent()) {
            UsernameNotFoundException usernameNotFoundException = new UnknownJwtIssuerException(clientKey);
            log.debug(usernameNotFoundException.getMessage());
            throw usernameNotFoundException;
        }
        return potentialHost.get();
    }

    private JWTClaimsSet verifyToken(JwtCredentials jwtCredentials, AtlassianHost host) throws AuthenticationException {
        String queryStringHash = computeQueryStringHash(jwtCredentials);
        JWTClaimsSet claims;
        try {
            claims = new JwtReader(host.getSharedSecret()).readAndVerify(jwtCredentials.getRawJwt(), queryStringHash);
        } catch (JwtParseException e) {
            log.error(e.getMessage());
            throw new InvalidJwtException(e.getMessage(), e);
        } catch (JwtExpiredException e) {
            log.error(e.getMessage());
            throw new CredentialsExpiredException(e.getMessage());
        } catch (JwtVerificationException e) {
            log.error(e.getMessage());
            throw new BadCredentialsException(e.getMessage(), e);
        }

        log.debug("Verified JWT for host {} ({}) ", host.getBaseUrl(), host.getClientKey());

        return claims;
    }

    private String computeQueryStringHash(JwtCredentials jwtCredentials) {
        CanonicalHttpRequest canonicalHttpRequest = jwtCredentials.getCanonicalHttpRequest();
        log.debug("Canonical request for incoming JWT: {}", CanonicalRequestUtil.toVerboseString(canonicalHttpRequest));

        try {
            return HttpRequestCanonicalizer.computeCanonicalRequestHash(canonicalHttpRequest);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
