package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.internal.jwt.JwtJsonBuilder;
import com.atlassian.connect.spring.internal.jwt.JwtWriter;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.crypto.MACSigner;

import java.time.Duration;

/**
 * A builder of JSON Web Tokens.
 */
public class JwtBuilder {

    private JwtJsonBuilder jwtJsonBuilder;

    private String sharedSecret;

    public JwtBuilder() {
        this.jwtJsonBuilder = new JwtJsonBuilder();
    }

    public JwtBuilder(Duration expireAfter) {
        this.jwtJsonBuilder = new JwtJsonBuilder(expireAfter);
    }

    public JwtBuilder issuer(String iss) {
        jwtJsonBuilder.issuer(iss);
        return this;
    }

    public JwtBuilder subject(String sub) {
        jwtJsonBuilder.subject(sub);
        return this;
    }

    public JwtBuilder audience(String aud) {
        jwtJsonBuilder.audience(aud);
        return this;
    }

    public JwtBuilder expirationTime(long exp) {
        jwtJsonBuilder.expirationTime(exp);
        return this;
    }

    public JwtBuilder notBefore(long nbf) {
        jwtJsonBuilder.notBefore(nbf);
        return this;
    }

    public JwtBuilder issuedAt(long iat) {
        jwtJsonBuilder.issuedAt(iat);
        return this;
    }

    public JwtBuilder queryHash(String queryHash) {
        jwtJsonBuilder.queryHash(queryHash);
        return this;
    }

    public JwtBuilder claim(String name, Object value) {
        jwtJsonBuilder.claim(name, value);
        return this;
    }

    public JwtBuilder signature(String sharedSecret) {
        this.sharedSecret = sharedSecret;
        return this;
    }

    public String build() {
        String jwtPayload = jwtJsonBuilder.build();
        return createJwtWriter().jsonToJwt(jwtPayload);
    }

    private JwtWriter createJwtWriter() {
        return new JwtWriter(JWSAlgorithm.HS256, new MACSigner(sharedSecret));
    }

    public String toString() {
        return build();
    }
}
