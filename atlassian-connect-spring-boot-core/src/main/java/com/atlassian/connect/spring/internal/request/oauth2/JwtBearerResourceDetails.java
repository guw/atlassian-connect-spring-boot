package com.atlassian.connect.spring.internal.request.oauth2;

import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;

import java.util.Collections;
import java.util.List;

public class JwtBearerResourceDetails implements OAuth2ProtectedResourceDetails {

    private static String JWT_BEARER_URN = "urn:ietf:params:oauth:grant-type:jwt-bearer";
    private final String clientKey;
    private final String secret;
    private final String authServerBaseUrl;

    public JwtBearerResourceDetails(String clientKey, String secret, String authServerBaseUrl) {
        this.clientKey = clientKey;
        this.secret = secret;
        this.authServerBaseUrl = authServerBaseUrl;
    }

    @Override
    public String getId() {
        return getClientId() + ":rest-apis";
    }

    @Override
    public String getClientId() {
        return clientKey;
    }

    @Override
    public String getAccessTokenUri() {
        return authServerBaseUrl + "/oauth2/token";
    }

    @Override
    public boolean isScoped() {
        return false;
    }

    @Override
    public List<String> getScope() {
        return Collections.emptyList();
    }

    @Override
    public boolean isAuthenticationRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return secret;
    }

    @Override
    public AuthenticationScheme getClientAuthenticationScheme() {
        return AuthenticationScheme.header;
    }

    @Override
    public String getGrantType() {
        return JWT_BEARER_URN;
    }

    @Override
    public AuthenticationScheme getAuthenticationScheme() {
        return AuthenticationScheme.header;
    }

    @Override
    public String getTokenName() {
        return "access_token";
    }

    @Override
    public boolean isClientOnly() {
        return true;
    }
}
