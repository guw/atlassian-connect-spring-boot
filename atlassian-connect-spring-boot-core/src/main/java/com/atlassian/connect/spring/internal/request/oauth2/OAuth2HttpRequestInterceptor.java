package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import com.atlassian.connect.spring.internal.request.AtlassianHostUriResolver;
import org.springframework.http.HttpRequest;

import java.net.URI;
import java.util.Optional;

public class OAuth2HttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private AtlassianHost host;

    public OAuth2HttpRequestInterceptor(AtlassianHost host, String atlassianConnectClientVersion) {
        super(atlassianConnectClientVersion);
        this.host = host;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        final URI requestUri = request.getURI();
        if (requestUri.isAbsolute() && !AtlassianHostUriResolver.isRequestToHost(requestUri, host)) {
            throw new UnsupportedOperationException("Aborting the request to "
                    + requestUri.toASCIIString() + requestUri.getPath() +
                    " because the user you're trying to make a request as does not reside on this host. "
                    + "The base url of the user's instance is " + host.getBaseUrl());
        }
        return Optional.of(host);
    }
}
