package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.AtlassianHosts;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.CLIENT_KEY;
import static com.atlassian.connect.spring.it.util.AtlassianHosts.SHARED_SECRET;
import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleJson;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LifecycleControllerBasicAuthIT extends BaseApplicationIT {

    @Value("${security.user.name}")
    private String adminUserName;

    @Value("${security.user.password}")
    private String adminUserPassword;

    @Test
    public void shouldRejectSharedSecretUpdateWithBasicAuth() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).sharedSecret(SHARED_SECRET)
                .baseUrl(AtlassianHosts.BASE_URL).build();
        hostRepository.save(host);
        mvc.perform(post("/custom-installed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("installed", SHARED_SECRET))
                .with(httpBasic(adminUserName, adminUserPassword)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldRejectUninstallWithBasicAuth() throws Exception {
        AtlassianHost host = new AtlassianHostBuilder().clientKey(CLIENT_KEY).sharedSecret(SHARED_SECRET)
                .baseUrl(AtlassianHosts.BASE_URL).build();
        hostRepository.save(host);
        mvc.perform(post("/custom-uninstalled")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createLifecycleJson("uninstalled", SHARED_SECRET))
                .with(httpBasic(adminUserName, adminUserPassword)))
                .andExpect(status().isUnauthorized());
    }
}
