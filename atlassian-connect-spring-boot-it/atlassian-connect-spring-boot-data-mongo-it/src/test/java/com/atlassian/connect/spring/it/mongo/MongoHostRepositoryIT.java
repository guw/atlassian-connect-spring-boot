package com.atlassian.connect.spring.it.mongo;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongoHostRepositoryIT {

    @Autowired
    private AtlassianHostRepository hostRepository;

    @After
    public void tearDown() {
        hostRepository.deleteAll();
    }

    @Test
    public void shouldHostRepositoryBeEmpty() throws Exception {
        assertThat(hostRepository.count(), is(0L));
    }

    @Test
    public void shouldHostRepositoryHaveOneHostAfterSavingOne() {
        createAndSaveHost(hostRepository);
        assertThat(hostRepository.count(), is(1L));
    }

    @Test
    public void shouldFindSavedHostByBaseUrl() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        Optional<AtlassianHost> foundHostOpt = hostRepository.findFirstByBaseUrl(host.getBaseUrl());
        assertThat(foundHostOpt, equalTo(of(host)));
    }

    @Test
    public void shouldFindSavedHostByClientKey() {
        AtlassianHost host = createAndSaveHost(hostRepository);
        AtlassianHost foundHost = hostRepository.findOne(host.getClientKey());
        assertThat(foundHost, equalTo(host));
    }
}
