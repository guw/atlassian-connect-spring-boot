package com.atlassian.connect.spring.it.lifecycle;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.it.util.AtlassianHostBuilder;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import static com.atlassian.connect.spring.it.util.LifecycleBodyHelper.createLifecycleEventMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyIterable.emptyIterable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LifecycleControllerTimeoutIT extends BaseApplicationIT {

    private static final Duration INSTALL_DURATION = Duration.of(4, ChronoUnit.SECONDS);

    private static final Duration CLIENT_TIMEOUT = Duration.of(10, ChronoUnit.SECONDS);

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Test
    public void shouldReturnServiceUnavailableOnServerTimeout() throws Exception {
        doAnswer(getInstallTimeoutAnswer()).when(hostRepository).findOne(any(String.class));
        AtlassianHost host = new AtlassianHostBuilder().build();
        RestTemplate restTemplate = createRestTemplateWithTimeout(host);
        ResponseEntity<String> response = restTemplate.postForEntity(URI.create(getServerAddress() + "/installed"),
                createLifecycleEventMap("installed"), String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.SERVICE_UNAVAILABLE));

        waitForInstallToFinish();
        assertThat(hostRepository.findAll(), is(emptyIterable()));
    }

    private RestTemplate createRestTemplateWithTimeout(AtlassianHost host) {
        ClientHttpRequestFactory requestFactory = createRequestFactoryWithTimeout();
        return restTemplateBuilder.requestFactory(requestFactory)
                .errorHandler(new NoopErrorHandler())
                .build();
    }

    private HttpComponentsClientHttpRequestFactory createRequestFactoryWithTimeout() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        int timeout = Math.toIntExact(CLIENT_TIMEOUT.toMillis());
        httpRequestFactory.setConnectionRequestTimeout(timeout);
        httpRequestFactory.setConnectTimeout(timeout);
        httpRequestFactory.setReadTimeout(timeout);
        return httpRequestFactory;
    }

    private Answer<AtlassianHost> getInstallTimeoutAnswer() {
        return invocation -> {
            try {
                Thread.sleep(INSTALL_DURATION.toMillis());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return null;
        };
    }

    private void waitForInstallToFinish() throws InterruptedException {
        Thread.sleep(INSTALL_DURATION.plus(1, ChronoUnit.SECONDS).toMillis());
    }

    /**
     * Workaround for https://github.com/spring-projects/spring-boot/issues/7033.
     */
    @TestConfiguration
    public static class MockRepositoryConfiguration {

        @Primary
        @Bean("atlassianHostRepositoryMock")
        public AtlassianHostRepository atlassianHostRepository(final AtlassianHostRepository hostRepository) {
            return mock(AtlassianHostRepository.class, AdditionalAnswers.delegatesTo(hostRepository));
        }
    }

    private static class NoopErrorHandler implements ResponseErrorHandler {

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            return false;
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {}
    }
}
