package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RootRedirectControllerIT extends BaseApplicationIT {

    @Test
    public void shouldRedirectRootToDescriptor() throws Exception {
        mvc.perform(get("/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/atlassian-connect.json"));
    }

    @Test
    public void shouldRedirectRootToDescriptorWithContextPath() throws Exception {
        String contextPath = "/addon";
        mvc.perform(get(contextPath + "/")
                .contextPath(contextPath)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl(contextPath + "/atlassian-connect.json"));
    }
}
