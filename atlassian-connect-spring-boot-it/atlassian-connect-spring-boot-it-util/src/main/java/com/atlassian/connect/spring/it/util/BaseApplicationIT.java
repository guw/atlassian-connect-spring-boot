package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.util.Optional;

public class BaseApplicationIT {

    protected MockMvc mvc;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected AtlassianHostRepository hostRepository;

    @Autowired
    private ConfigurableEnvironment environment;

    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
        hostRepository.deleteAll();
    }

    protected String getServerAddress() {
        String port = environment.getProperty("local.server.port", "8080");
        return "http://localhost:" + port;
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host) {
        setJwtAuthenticatedPrincipal(host, null);
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host, String userKey) {
        AtlassianHostUser hostUser = new AtlassianHostUser(host, Optional.ofNullable(userKey));
        TestingAuthenticationToken authentication = new TestingAuthenticationToken(hostUser, null);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
